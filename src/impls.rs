use crate::{as_maybeuninit_slice_mut, Buffer};
use std::io;

impl io::Read for Buffer {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        if self.filled_len() == 0 {
            return Err(io::ErrorKind::WouldBlock.into());
        }
        let buf = as_maybeuninit_slice_mut(buf);
        Ok(self.pull(buf))
    }

    fn read_vectored(&mut self, bufs: &mut [io::IoSliceMut<'_>]) -> io::Result<usize> {
        if self.filled_len() == 0 {
            return Err(io::ErrorKind::WouldBlock.into());
        }
        let mut total_bytes = 0;
        for buf in bufs {
            let buf = as_maybeuninit_slice_mut(buf);
            total_bytes += self.pull(buf);
        }
        Ok(total_bytes)
    }

    fn read_to_end(&mut self, buf: &mut Vec<u8>) -> io::Result<usize> {
        let chunk = self.filled();
        let len = chunk.len();

        buf.extend_from_slice(chunk);
        self.advance_head(len);
        Ok(len)
    }

    fn read_to_string(&mut self, buf: &mut String) -> io::Result<usize> {
        let chunk = self.filled();
        let len = chunk.len();

        let chunk = core::str::from_utf8(chunk).map_err(|_| {
            io::Error::new(
                io::ErrorKind::InvalidData,
                "buffer did not contain valid UTF-8",
            )
        })?;

        buf.push_str(chunk);
        self.advance_head(len);
        Ok(len)
    }
}

impl io::Write for Buffer {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        if self.unfilled_len() == 0 {
            return Err(io::ErrorKind::WouldBlock.into());
        }
        Ok(self.push(buf))
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }

    fn write_vectored(&mut self, bufs: &[io::IoSlice<'_>]) -> io::Result<usize> {
        if self.unfilled_len() == 0 {
            return Err(io::ErrorKind::WouldBlock.into());
        }
        let mut total_bytes = 0;
        for buf in bufs {
            total_bytes += self.push(buf);
        }
        Ok(total_bytes)
    }
}
