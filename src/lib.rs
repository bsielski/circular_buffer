use core::cmp::{max, min};
use core::mem::{self, MaybeUninit};
use core::slice;
use std::io;

mod impls;
mod sys;

// source https://graphics.stanford.edu/%7Eseander/bithacks.html#RoundUpPowerOf2
pub(crate) const fn round_up_to_pow_2(mut value: usize) -> usize {
    value -= 1;
    let mut i = 1;
    while i < mem::size_of::<usize>() * 8 {
        value |= value >> i;
        i *= 2;
    }
    value + 1
}

pub(crate) const fn as_maybeuninit_slice<T>(val: &[T]) -> &[MaybeUninit<T>] {
    // SAFETY: &[T] and &[MaybeUninit<T>] have the same layout
    unsafe { mem::transmute(val) }
}

pub(crate) fn as_maybeuninit_slice_mut<T>(val: &mut [T]) -> &mut [MaybeUninit<T>] {
    // SAFETY: &mut [T] and &mut [MaybeUninit<T>] have the same layout
    unsafe { mem::transmute(val) }
}

#[derive(Debug)]
pub struct Buffer {
    inner: sys::Buffer,
    tail: u64,
    head: u64,
    initialized: usize,
}

impl Buffer {
    /// Create a new `Buffer` with at least the specified size.
    pub fn new(size: usize) -> io::Result<Self> {
        sys::Buffer::new(size).map(|inner| Self {
            inner,
            tail: 0,
            head: 0,
            initialized: 0,
        })
    }

    /// Resize the `Buffer` to at least the specified size.  If the new `size` is smaller than
    /// `Buffer::filled_len` data might be lost.
    ///
    /// Note that resizing the buffer requires all the data to be copied over so avoid doing
    /// this often.
    pub fn resize(&mut self, size: usize) -> io::Result<()> {
        let size = sys::Buffer::adjust_size(size);
        if size == self.size() {
            // Nothing to do.
            return Ok(());
        }

        let mut new = Self::new(size)?;
        new.push(self.filled());
        mem::swap(self, &mut new);
        Ok(())
    }

    /// Returns the size of the buffer.
    pub const fn size(&self) -> usize {
        self.inner.size
    }

    /// Returns the length of the unfilled part of the buffer.
    pub const fn unfilled_len(&self) -> usize {
        self.size() - self.filled_len()
    }

    /// Returns a mutable reference to the unfilled portion of the buffer which will
    /// first be initialized to 0 if it wasn't previously.
    pub fn unfilled(&mut self) -> &mut [u8] {
        let offset = (self.tail as usize) & (self.size() - 1);
        let len = self.unfilled_len();
        let to_init = (offset + len).saturating_sub(self.initialized);
        unsafe {
            let ptr = self.inner.data.as_ptr().add(offset) as *mut u8;
            ptr.add(self.initialized).write_bytes(0, to_init);
            self.initialized += to_init;
            slice::from_raw_parts_mut(ptr, len)
        }
    }

    /// Returns a mutable reference to the unfilled portion of the buffer which may
    /// not be initialized.
    pub fn unfilled_uninit(&mut self) -> &mut [MaybeUninit<u8>] {
        let offset = (self.tail as usize) & (self.size() - 1);
        let len = self.unfilled_len();
        unsafe {
            let ptr = self.inner.data.as_ptr().add(offset);
            slice::from_raw_parts_mut(ptr, len)
        }
    }

    /// Assert that `count` bytes of the unfilled portion of the buffer have been filled.  If
    /// [`Buffer::unfilled`] was used to get a mutable reference to the unfilled portion this is
    /// always safe.
    ///
    /// # Panics
    ///
    /// Panics if `count` is larger than `self.unfilled_len()`
    pub unsafe fn advance_tail(&mut self, count: usize) {
        let offset = (self.tail as usize) & (self.size() - 1);
        self.initialized = max(offset + count, self.initialized);

        assert!(count <= self.unfilled_len());
        self.tail = self.tail.wrapping_add(count as u64);
    }

    /// Returns the length of the filled part of the buffer.
    pub const fn filled_len(&self) -> usize {
        self.tail.wrapping_sub(self.head) as usize
    }

    /// Returns a shared reference to the filled portion of the buffer.
    pub const fn filled(&self) -> &[u8] {
        let offset = (self.head as usize) & (self.size() - 1);
        unsafe {
            let ptr = self.inner.data.as_ptr().add(offset) as *const u8;
            slice::from_raw_parts(ptr, self.filled_len())
        }
    }

    /// Reduce the filled portion of the buffer by `count` bytes.
    ///
    /// # Panics
    ///
    /// Panics if `count` is larger than `self.filled_len()`
    pub fn advance_head(&mut self, count: usize) {
        assert!(count <= self.filled_len());
        self.head = self.head.wrapping_add(count as u64);
    }

    /// Push bytes onto the tail of the buffer, returning how many bytes were pushed.
    ///
    /// Essentially this is an infallible version of [`std::io::Write::write`], but note
    /// that a `0` return value does not mean that the buffer will no longer accepts
    /// bytes, just that it is currently full.
    pub fn push(&mut self, buf: &[u8]) -> usize {
        let len = min(self.unfilled_len(), buf.len());
        let buf = as_maybeuninit_slice(&buf[..len]);
        self.unfilled_uninit()[..len].copy_from_slice(buf);
        // SAFETY: `len` bytes were just written into the internal buffer
        unsafe { self.advance_tail(len) };
        len
    }

    /// Pull bytes from the head of the buffer, returning how many bytes were pulled.
    ///
    /// Essentially this is an infallible version of [`std::io::Read::read`], but note
    /// that a `0` return value does not mean that the buffer will no longer produce
    /// bytes, just that it is currently empty.
    pub fn pull(&mut self, buf: &mut [MaybeUninit<u8>]) -> usize {
        let len = min(self.filled_len(), buf.len());
        let filled = as_maybeuninit_slice(&self.filled()[..len]);
        buf[..len].copy_from_slice(filled);
        self.advance_head(len);
        len
    }

    #[cfg(test)]
    const fn slice(&self, offset: usize, len: usize) -> &[u8] {
        unsafe {
            let ptr = self.inner.data.as_ptr().add(offset) as *const u8;
            slice::from_raw_parts(ptr, len)
        }
    }
}

unsafe impl Send for Buffer {}
unsafe impl Sync for Buffer {}

#[cfg(test)]
mod tests {
    use super::*;

    #[ctor::ctor]
    fn test_init() {
        env_logger::builder()
            .filter_level(log::LevelFilter::Trace)
            .is_test(true)
            .init();
    }

    #[test]
    fn basic() {
        let mut buffer = Buffer::new(0).unwrap();

        const DATA: &[u8] = b"0123456789";
        assert_eq!(buffer.push(DATA), DATA.len());

        let tail = buffer.filled();
        assert_eq!(tail, DATA);

        assert_eq!(buffer.initialized, DATA.len());

        // Check that the data is available twice in the buffers internal memory
        let first_half_slice = buffer.slice(0, DATA.len());
        assert_eq!(first_half_slice, DATA);

        let second_half_slice = buffer.slice(buffer.size(), DATA.len());
        assert_eq!(second_half_slice, DATA);
    }

    #[test]
    fn at_end() {
        let mut buffer = Buffer::new(0).unwrap();
        buffer.tail = buffer.size() as u64 - 5;
        buffer.head = buffer.size() as u64 - 5;
        assert_eq!(buffer.filled_len(), 0);

        const DATA: &[u8] = b"0123456789";
        assert_eq!(buffer.push(DATA), DATA.len());

        let tail = buffer.filled();
        assert_eq!(tail, DATA);

        let buffer_slice = buffer.slice(0, buffer.size() * 2);
        assert_eq!(&buffer_slice[..(DATA.len() - 5)], &DATA[5..]);
    }

    #[test]
    fn overflow() {
        let mut buffer = Buffer::new(0).unwrap();
        buffer.tail = u64::MAX - 16;
        buffer.head = u64::MAX - 16;

        unsafe {
            buffer.advance_tail(32);
        }
        assert_eq!(buffer.filled_len(), 32);
        assert_eq!(buffer.unfilled_len(), buffer.size() - 32);

        buffer.advance_head(10);
        assert_eq!(buffer.filled_len(), 22);
        assert_eq!(buffer.unfilled_len(), buffer.size() - 22);

        buffer.advance_head(10);
        assert_eq!(buffer.filled_len(), 12);
        assert_eq!(buffer.unfilled_len(), buffer.size() - 12);

        buffer.advance_head(10);
        assert_eq!(buffer.filled_len(), 2);
        assert_eq!(buffer.unfilled_len(), buffer.size() - 2);
    }

    #[test]
    fn resize_up() {
        let mut buffer = Buffer::new(512).unwrap();
        const DATA: &[u8] = b"0123456789abcdef";

        while (buffer.tail as usize) < buffer.size() {
            buffer.push(DATA);
        }
        buffer.push(DATA);

        let mut buf = [0u8; 317];
        assert_eq!(buffer.pull(as_maybeuninit_slice_mut(&mut buf)), buf.len());

        assert_eq!(&buf[..16], DATA);
        assert_eq!(&buf[304..], &DATA[..13]);
        assert_eq!(&buffer.filled()[..16], b"def0123456789abc");

        let used_before_resize = buffer.filled_len();
        eprintln!("{:?}", buffer);
        buffer.resize(buffer.size() * 4).unwrap();
        eprintln!("{:?}", buffer);

        assert_eq!(buffer.filled_len(), used_before_resize);
        assert_eq!(&buffer.filled()[..16], b"def0123456789abc");
    }

    #[test]
    fn initialization() {
        let mut buffer = Buffer::new(0).unwrap();

        eprintln!("{:02x?}", buffer.slice(0, buffer.size() * 2));

        let _unfilled = buffer.unfilled();
        assert_eq!(buffer.initialized, buffer.size());

        eprintln!("{:02x?}", buffer.slice(0, buffer.size() * 2));

        const DATA: &[u8] = b"0123456789";
        assert_eq!(buffer.push(DATA), DATA.len());

        eprintln!("{:02x?}", buffer.slice(0, buffer.size() * 2));

        let mut buf = [0u8; DATA.len()];
        assert_eq!(buffer.pull(as_maybeuninit_slice_mut(&mut buf)), buf.len());

        for byte in buffer.unfilled() {
            assert_ne!(*byte, 0xFF);
        }
    }
}
