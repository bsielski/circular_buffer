use core::mem::MaybeUninit;
use core::ptr::NonNull;
use std::io;
use std::os::unix::io::{FromRawFd, OwnedFd};
use std::sync::atomic::{self, AtomicUsize};
use std::sync::OnceLock;

#[derive(Debug)]
pub struct Buffer {
    pub data: NonNull<MaybeUninit<u8>>,
    pub size: usize,
}

impl Buffer {
    fn _new(
        size: usize,
        memfd_create_flags: libc::c_uint,
        mmap_flags: libc::c_int,
    ) -> io::Result<Self> {
        unsafe {
            let fd = libc::memfd_create(
                b"circular-buffer\0" as *const _ as *const _,
                memfd_create_flags | libc::MFD_CLOEXEC,
            );
            if fd < 0 {
                return Err(io::Error::last_os_error());
            }
            // Convert to an OwnedFd so the fd is cleaned up automatically.
            // Once the fd has been mapped it is no longer needed.
            let _owned_fd = OwnedFd::from_raw_fd(fd);

            if libc::ftruncate(fd, size as libc::off_t) < 0 {
                return Err(io::Error::last_os_error());
            }

            let data = libc::mmap(
                core::ptr::null_mut(),
                size * 2,
                libc::PROT_NONE,
                mmap_flags | libc::MAP_PRIVATE | libc::MAP_ANONYMOUS,
                -1,
                0,
            );
            if data == libc::MAP_FAILED {
                return Err(io::Error::last_os_error());
            }
            let data = NonNull::new_unchecked(data as *mut _);
            // Create the return value here to help with cleanup if an error occurs.
            let ret = Self { data, size };

            if libc::mmap(
                ret.data.as_ptr() as *mut _,
                size,
                libc::PROT_READ | libc::PROT_WRITE,
                mmap_flags | libc::MAP_SHARED | libc::MAP_FIXED,
                fd,
                0,
            ) == libc::MAP_FAILED
            {
                return Err(io::Error::last_os_error());
            }

            if libc::mmap(
                ret.data.as_ptr().add(size) as *mut _,
                size,
                libc::PROT_READ | libc::PROT_WRITE,
                mmap_flags | libc::MAP_SHARED | libc::MAP_FIXED,
                fd,
                0,
            ) == libc::MAP_FAILED
            {
                return Err(io::Error::last_os_error());
            }

            #[cfg(test)]
            {
                let ptr = ret.data.as_ptr() as *mut u8;
                ptr.write_bytes(0xFF, size);
            }

            Ok(ret)
        }
    }

    pub fn adjust_size(size: usize) -> usize {
        let page_size = page_size();
        if size < page_size {
            page_size
        } else {
            crate::round_up_to_pow_2(size)
        }
    }

    pub fn new(size: usize) -> io::Result<Self> {
        let size = Self::adjust_size(size);

        for page_size in huge_tlb_page_sizes() {
            let PageSize {
                size: page_size,
                memfd_create_flags,
                mmap_flags,
            } = *page_size;

            if page_size > size {
                continue;
            }

            match Self::_new(size, memfd_create_flags, mmap_flags) {
                Err(e) if e.kind() == io::ErrorKind::OutOfMemory => continue,
                ret => return ret,
            }
        }

        Self::_new(size, 0, 0)
    }
}

impl Drop for Buffer {
    fn drop(&mut self) {
        unsafe {
            libc::munmap(self.data.as_ptr() as *mut _, self.size * 2);
        }
    }
}

fn page_size() -> usize {
    static PAGE_SIZE: AtomicUsize = AtomicUsize::new(0);

    let page_size = PAGE_SIZE.load(atomic::Ordering::Relaxed);
    if page_size != 0 {
        return page_size;
    }

    unsafe {
        let ret = libc::sysconf(libc::_SC_PAGESIZE);
        if ret < 0 {
            panic!("Failed to read page size: {}", io::Error::last_os_error());
        }
        let ret = ret as usize;
        if ret == 0 {
            panic!("Page size is 0");
        }
        if ret & (ret - 1) != 0 {
            panic!("Page size is not a power of 2 (page size = {})", ret);
        }
        PAGE_SIZE.store(ret, atomic::Ordering::Relaxed);
        ret
    }
}

#[derive(Clone, Copy, Debug)]
struct PageSize {
    size: usize,
    memfd_create_flags: libc::c_uint,
    mmap_flags: libc::c_int,
}

fn huge_tlb_page_sizes() -> &'static [PageSize] {
    static PAGE_SIZES: OnceLock<Box<[PageSize]>> = OnceLock::new();

    fn get_page_sizes() -> io::Result<Box<[PageSize]>> {
        let mut page_sizes: Vec<PageSize> = Vec::new();

        for direntry in std::fs::read_dir("/sys/kernel/mm/hugepages")? {
            let direntry = direntry?;
            if !direntry.file_type()?.is_dir() {
                continue;
            }

            let name = direntry.file_name();
            let name = match name.to_str() {
                Some(name) => name,
                None => {
                    log::trace!("Ignoring /sys/kernel/mm/hugepages subdir because it contains non-UTF-8 characters");
                    continue;
                }
            };

            let size = match name
                .strip_prefix("hugepages-")
                .and_then(|size| size.strip_suffix("kB"))
            {
                Some(size) => size,
                None => {
                    log::trace!(
                        "Ignoring directory \"{}\" because it does not match the expected patern (which is hugepages-{{size}}kB)",
                        name,
                    );
                    continue;
                }
            };

            let size: usize = match size.parse() {
                Ok(size) => size,
                Err(e) => {
                    log::debug!(
                        "Failed to parse page size from directory name \"{}\": {}",
                        name,
                        e,
                    );
                    continue;
                }
            };
            let size = size * 1024;

            let size_log2 = match size.checked_ilog2() {
                Some(size_log2) => size_log2,
                None => {
                    log::debug!(
                        "Failed to parse page size from directory name \"{}\": 0 page size",
                        name,
                    );
                    continue;
                }
            };

            page_sizes.push(PageSize {
                size,
                memfd_create_flags: libc::MFD_HUGETLB
                    | ((size_log2 as libc::c_uint) << libc::MFD_HUGE_SHIFT),
                mmap_flags: libc::MAP_HUGETLB
                    | ((size_log2 as libc::c_int) << libc::MAP_HUGE_SHIFT),
            });
        }

        page_sizes.sort_by(|a, b| a.size.cmp(&b.size).reverse());
        Ok(page_sizes.into())
    }

    let page_sizes = PAGE_SIZES.get_or_init(|| get_page_sizes().unwrap_or_default());
    page_sizes
}
